/*------------------------------------------------

Implementation of BFS and DFS

author: Febrian Rendak
email : febriananugrah92@gmail.com
git   : https://bitbucket.org/febrianrendak/pasd-bfs-dfs/

--------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

typedef struct Node
{
    char node_name;
    int start; //TRUE if it's start node, otherwise;
    int target; ////TRUE if it's target node, otherwise;
    int explore;
    struct List *adjacent;
    struct Node *next_node;
} Node;

typedef struct List
{
    struct Node *node;
    struct List *next_list;
} List;

typedef struct NodeHandler
{
    struct Node *node;
} NodeHandler;

typedef struct ListHandler
{
    struct List *list;
} ListHandler;

Node *new_node(char node_name, List *adjacent,  Node *next_node)
{
    Node *new_node = (Node*)malloc(sizeof(Node));
    new_node->node_name = node_name;
    new_node->start = FALSE;
    new_node->target = FALSE;
    new_node->explore = FALSE;
    new_node->adjacent = adjacent;
    new_node->next_node = next_node;

    return(new_node);
}

List *new_list(Node *node, List *next_list)
{
    List *new_list = (List*)malloc(sizeof(List));
    new_list->node = node;
    new_list->next_list = next_list;

    return(new_list);
}

NodeHandler *new_node_handler(Node *node)
{
    NodeHandler *new_node_handler = (NodeHandler*)malloc(sizeof(NodeHandler));
    new_node_handler->node = node;

    return(new_node_handler);
}

ListHandler *new_list_handler(List *list)
{
    ListHandler *new_list_handler = (ListHandler*)malloc(sizeof(ListHandler));
    new_list_handler->list = list;

    return(new_list_handler);
}

void add_node(NodeHandler *node_handler, char node_name)
{
    if(node_handler->node)
    {
        Node *temp_node = node_handler->node;

        while(temp_node->next_node)
            temp_node = temp_node->next_node;

        temp_node->next_node = new_node(node_name, NULL, NULL);
    }
    else
    {
        node_handler->node = new_node(node_name, NULL, NULL);
    }
}

void add_node_to_adjacent(Node *node, Node *node_adjacent)
{
    if(node->adjacent)
    {
        List *temp_adjacent = node->adjacent;
        while(temp_adjacent->next_list)
            temp_adjacent = temp_adjacent->next_list;

        temp_adjacent->next_list = new_list(node_adjacent, NULL);
    }
    else
    {
        node->adjacent  = new_list(node_adjacent, NULL);
    }
}

void add_node_to_list_head(ListHandler *list_handler, Node *node)
{
    if(list_handler->list)
    {
        List *temp_list = new_list(node, list_handler->list);
        list_handler->list = temp_list;
    }
    else
    {
        list_handler->list = new_list(node, NULL);
    }
}

void add_node_to_list_tails(ListHandler *list_handler, Node *node)
{
    if(list_handler->list)
    {
        List *temp_list = list_handler->list;

        while(temp_list->next_list)
            temp_list = temp_list->next_list;

        temp_list->next_list = new_list(node, NULL);
    }
    else
    {
        list_handler->list = new_list(node, NULL);
    }
}

Node *remove_node_at_list_head(ListHandler *list_handler)
{
    if(list_handler->list)
    {
        Node *temp_node = list_handler->list->node;
        free(list_handler->list);
        list_handler->list = list_handler->list->next_list;
        return(temp_node);
    }
    else
    {
        return(NULL);
    }
}

void print_node(Node *node)
{
    printf("%c ", node->node_name);
}

void print_nodes(Node *node)
{
    Node *temp_node = node;

    while(temp_node)
    {
        print_node(temp_node);
        temp_node = temp_node->next_node;
    }
}

void print_list(List *list)
{
    List *temp_list = list;

    if(temp_list)
    {
        while(temp_list)
        {
            print_node(temp_list->node);
            temp_list = temp_list->next_list;
        }
    }
    else
    {
        printf("-");
    }
}

void print_node_adjacent(Node *node)
{
    Node *temp_node = node;

    while(temp_node)
    {
        printf("Node: "); print_node(temp_node);
        printf("\n\tAdjacent: "); print_list(temp_node->adjacent);
        printf("\n\n");

        temp_node = temp_node->next_node;
    }
}

int is_valid_char(char check)
{
    if((check >= 65 && check <= 90) || (check >= 97 && check <= 122)) //return TRUE if char is a-z/A-Z
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

int is_node_exist_at_nodes(Node *node, char node_name) //find node from
{
    int isFound = FALSE;
    Node *temp_node = node;

    while(temp_node)
    {
        if(temp_node->node_name == node_name)
        {
            isFound = TRUE;
            break;
        }
        temp_node = temp_node->next_node;
    }

    return(isFound);
}

int is_node_exist_at_list(List *list, char node_name)
{
    int isFound = FALSE;
    List *temp_list = list;

    while(temp_list)
    {
        if(temp_list->node->node_name == node_name)
        {
            isFound = TRUE;
            break;
        }
        temp_list = temp_list->next_list;
    }

    return(isFound);
}

Node *get_node_from_nodes(Node *node, char node_name)
{
    Node *return_node = NULL;
    Node *temp_node = node;

    while(temp_node)
    {
        if(temp_node->node_name == node_name)
        {
            return_node = temp_node;
            break;
        }
        temp_node = temp_node->next_node;
    }

    return(return_node);
}

Node *get_node_from_list(List *list, char node_name)
{
    Node *return_node = NULL;
    List *temp_list = list;

    while(temp_list)
    {
        if(temp_list->node->node_name == node_name)
        {
            return_node = temp_list->node;
            break;
        }
        temp_list = temp_list->next_list;
    }

    return(return_node);
}

int count_node(Node *node)
{
    int count = 0;
    Node *temp_node = node;

    while(temp_node)
    {
        count++;
        temp_node = temp_node->next_node;
    }

    return(count);
}

int count_list(List *list)
{
    int count = 0;
    List *temp_list = list;

    while(temp_list)
    {
        count++;
        temp_list = temp_list->next_list;
    }

    return(count);
}

void read_line(NodeHandler *node_handler, char line[])
{
    int i = 0, node_target_found = FALSE;
    char node_target_c, node_adjacent_c;
    for(i = 0; i < strlen(line); i++)
    {
        if(is_valid_char(line[i])) //check valid char
        {
            //printf("%c ", line[i]);
            if(!node_target_found) //if node_target not found, set char line to node_target and set TRUE to node_target_found
            {
                node_target_c = toupper(line[i]);
                node_target_found = TRUE;

                if(!is_node_exist_at_nodes(node_handler->node, node_target_c)) //if node_target not exist at nodes then add node_target to nodes
                {
                    add_node(node_handler, node_target_c);
                }
            }
            else //if node_target was found
            {
                node_adjacent_c = toupper(line[i]);
                if(!is_node_exist_at_nodes(node_handler->node, node_adjacent_c)) //if the next node after node_target not exist at nodes, then add node to nodes
                {
                    add_node(node_handler, node_adjacent_c);
                }

                Node *node_target = get_node_from_nodes(node_handler->node, node_target_c);
                if(!is_node_exist_at_list(node_target->adjacent, node_adjacent_c)) //if node is not adjacent from node_target, then add it to node_target's adjacent list
                {
                    Node *node_adjacent = get_node_from_nodes(node_handler->node, node_adjacent_c);
                    add_node_to_adjacent(node_target, node_adjacent);
                }
            }
        }
    }
}

int read_file(NodeHandler *node_handler, char file_name[])
{
    int is_read = FALSE;

    FILE *file = fopen(file_name, "r");

    if(file)
    {
        is_read = TRUE;

        char line[1024];
        while(fgets(line, sizeof line, file) != NULL)
        {
            read_line(node_handler, line);
        }

        fclose(file);
    }

    return(is_read);
}

int is_start_node(Node *node)
{
    if(node->start)
        return(TRUE);
    else
        return(FALSE);
}

int is_target_node(Node *node)
{
    if(node->target)
        return(TRUE);
    else
        return(FALSE);
}

int is_explored(Node *node)
{
    if(node->explore)
        return(TRUE);
    else
        return(FALSE);
}

void set_node_as_start_node(Node *node)
{
    node->start = TRUE;
}

void set_node_as_target_node(Node *node)
{
    node->target = TRUE;
}

void set_node_as_explored(Node *node)
{
    node->explore = TRUE;
}

void set_nodes_as_unexplored(Node *node)
{
    Node *temp_node = node;

    while(temp_node)
    {
        node->explore = FALSE;
        temp_node = temp_node->next_node;
    }
}

void set_start_node(NodeHandler *node_handler)
{
    int is_start_node_set = FALSE;
    char node_name[256];

    while(!is_start_node_set)
    {
        printf("Start Node: "); scanf("%[^\n]", node_name);

        if(strlen(node_name) == 1 && is_node_exist_at_nodes(node_handler->node, toupper(node_name[0])))
        {
            Node *temp_node = get_node_from_nodes(node_handler->node, toupper(node_name[0]));
            set_node_as_start_node(temp_node);
            is_start_node_set = TRUE;
        }
        else
        {
            printf("\n\nERROR: Node not found, try again!\n\n");
        }

        getchar();
    }
}

void set_target_node(NodeHandler *node_handler)
{
    int node_target_count = 0;
    int stop = FALSE;
    char node_name[256];

    while(!stop)
    {
        //printf("1");
        printf("Target Node - %i: ", node_target_count+1); scanf("%[^\n]", node_name);

        if(strlen(node_name) == 1 && is_node_exist_at_nodes(node_handler->node, toupper(node_name[0])))
        {
            Node *temp_node = get_node_from_nodes(node_handler->node, toupper(node_name[0]));
            if(!is_target_node(temp_node) && !is_start_node(temp_node))
            {
                set_node_as_target_node(temp_node);
                node_target_count++;
            }
            else
            {
                printf("\n\nERROR: Node was set as Target Node or Start Node, try again!\n\n");
            }
        }
        else
        {
            printf("\n\nERROR: Node not found, try again!\n\n");
        }

        getchar();

        if(node_target_count > 0)
        {
            char choice[256];
            printf("Add more Target Node (y. Yes | n. No) ? "); scanf("%[^\n]", choice);

            if(toupper(choice[0]) == 'Y')
                getchar();
            else
                stop = TRUE;
        }
    }
}

void print_start_node(Node *node)
{
    Node *temp_node = node;
    while(temp_node)
    {
        if(is_start_node(temp_node))
        {
            printf("%c ", temp_node->node_name);
        }

        temp_node = temp_node->next_node;
    }
}

void print_target_node(Node *node)
{
    Node *temp_node = node;
    while(temp_node)
    {
        if(is_target_node(temp_node))
        {
            printf("%c ", temp_node->node_name);
        }

        temp_node = temp_node->next_node;
    }
}

Node *get_start_node(Node *node)
{
    Node *return_node = NULL;
    Node *temp_node = node;

    while(temp_node)
    {
        if(is_start_node(node))
        {
            return_node = node;
            break;
        }
        temp_node = temp_node->next_node;
    }

    return(return_node);
}

void open_bfs(ListHandler *queue, ListHandler *garbage_list, Node *node)
{
    List *temp_list = node->adjacent;

    while(temp_list)
    {
        if(!is_node_exist_at_list(queue->list, temp_list->node->node_name) && !is_node_exist_at_list(garbage_list->list, temp_list->node->node_name))
        {
            add_node_to_list_tails(queue, temp_list->node);
        }

        temp_list = temp_list->next_list;
    }

}

void bfs(NodeHandler *node_handler)
{
    int is_target_found = FALSE;
    Node *start_node = get_start_node(node_handler->node);

    ListHandler *queue = new_list_handler(NULL);
    ListHandler *garbage_list = new_list_handler(NULL);

    add_node_to_list_tails(queue, start_node);
    while(1)
    {
        Node *open_node = remove_node_at_list_head(queue);
        if(open_node && !is_target_found)
        {
            if(is_target_node(open_node))
            {
                printf("Open Node\t: "); print_node(open_node);
                printf("\n\nTarget Node found : %c.\n\nProcess Finish. Exit...", open_node->node_name);
                is_target_found = TRUE;
            }
            else
            {
                add_node_to_list_tails(garbage_list, open_node);

                open_bfs(queue, garbage_list, open_node);

                printf("Open Node\t: "); print_node(open_node);
                printf("\nAdjacent\t: "); if(open_node->adjacent) print_list(open_node->adjacent); else printf("-");
                printf("\nQueue\t\t: "); if(queue->list) print_list(queue->list); else printf("-");
                printf("\n\n");
            }
        }
        else
        {
            break;
        }
    }

    if(!is_target_found)
    {
        printf("\n\nTarget Node Not Found!!!\n\n");
    }
}

void dfs_explore_next_node_adjacent(ListHandler *stack, Node *node)
{
    int end = FALSE;
    Node *temp_node = node;

    while(!end)
    {
        if(temp_node)
        {
            if(!is_explored(temp_node))
            {
                set_node_as_explored(temp_node);
                add_node_to_list_head(stack, temp_node);

                if(temp_node->adjacent)
                {
                    printf("Open Node\t: "); print_node(temp_node);
                    printf("\nAdjacent\t: "); if(temp_node->adjacent) print_list(temp_node->adjacent); else printf("-");
                    printf("\nStack\t\t: "); if(stack->list) print_list(stack->list); else printf("-");
                    printf("\n\n");
                    temp_node = temp_node->adjacent->node;
                }
            }
            else
            {
                end = TRUE;
            }
        }
        else
        {
            end = TRUE;
        }
    }
}

void dfs_explore_adjacent(ListHandler *stack, Node *node)
{
    List *adjacent = node->adjacent;

    while(adjacent)
    {
        if(!is_explored(adjacent->node))
        {
            dfs_explore_next_node_adjacent(stack, adjacent->node);
        }

        adjacent = adjacent->next_list;
    }
}

void dfs(NodeHandler *node_handler)
{

    int is_target_found = FALSE;
    Node *start_node = get_start_node(node_handler->node);
    Node *open_node = NULL;
    ListHandler *stack = new_list_handler(NULL);

    add_node_to_list_head(stack, start_node);

    while(stack->list && !is_target_found)
    {
        open_node = remove_node_at_list_head(stack);

        if(is_target_node(open_node))
        {
            printf("Open Node\t: "); print_node(open_node);
            printf("\n\nTarget Node found : %c.\n\nProcess Finish. Exit...", open_node->node_name);
            is_target_found = TRUE;
        }
        else
        {
            printf("Open Node\t: "); print_node(open_node);
            printf("\nAdjacent\t: "); if(open_node->adjacent) print_list(open_node->adjacent); else printf("-");
            printf("\nStack\t\t: "); if(stack->list) print_list(stack->list); else printf("-");
            printf("\n\n");

            dfs_explore_adjacent(stack, open_node);
        }
    }

    if(!is_target_found)
    {
        printf("\n\nTarget Node Not Found!!!\n\n");
    }
}

int main(int argc, char *argv[])
{
    printf("BFS & DFS\n\n");

    NodeHandler *node_handler = new_node_handler(NULL);
    if(read_file(node_handler, "input.txt"))
    {
        printf("Print All Node and Adjacent : \n\n");
        print_node_adjacent(node_handler->node);

        if(count_node(node_handler->node) > 1)
        {
            printf("\n\nSet Start Node and Target Node :\n\n");
            set_start_node(node_handler);
            //getchar();
            set_target_node(node_handler);

            printf("\n\nStart Node\t: "); print_start_node(node_handler->node);
            printf("\n\nTarget Node\t: "); print_target_node(node_handler->node);

            printf("\n\nBegin BFS Process...\n\n");
            bfs(node_handler);

            set_nodes_as_unexplored(node_handler->node); //clear all explore mark, set to FALSE
            printf("\n\nBegin DFS Process...\n\n");
            dfs(node_handler);
        }
        else
        {
            printf("Node must more than 1...\n\n");
        }
    }
    else
    {
        printf("Can't read file, not found or file format error...\n\n");
    }

    return(0);
}
